from keras.models import load_model
from keras.preprocessing import image
from keras import backend as K
from urllib import request
import os
import numpy as np
from PIL import Image

    
def ImageConvert(n, i):
    img_x = img_y = 50
    im_ex = i.reshape(n, img_x, img_y, 3)
    im_ex = im_ex.astype('float32') / 255
    im_ex = np.subtract(im_ex, 0.5)
    im_ex = np.multiply(im_ex, 2.0)
    return im_ex

def testing(url):
    K.clear_session()
    imgname = 'test_img.jpg'
    f = open(imgname, 'wb')
    f.write(request.urlopen(url).read())
    f.close()
    test_image = image.load_img('test_img.jpg', target_size = (50, 50))
    test_image = image.img_to_array(test_image)
    test_image = np.expand_dims(test_image, axis = 0)

    cars = ['Alfa Romeo', 'Audi', 'BMW', 'Chevrolet', 'Citroen', 'Dacia', 'Daewoo', 'Dodge',
            'Ferrari', 'Fiat', 'Ford', 'Honda', 'Hyundai', 'Jaguar', 'Jeep', 'Kia', 'Lada',
            'Lancia', 'Land Rover', 'Lexus', 'Maserati', 'Mazda', 'Mercedes', 'Mitsubishi',
            'Nissan', 'Opel', 'Peugeot', 'Porsche', 'Renault', 'Rover', 'Saab', 'Seat',
            'Skoda', 'Subaru', 'Suzuki', 'Tata', 'Tesla', 'Toyota', 'Volkswagen', 'Volvo']
    
    model = load_model('saved_model_3.h5py')
    Companies = []
    m = int(model.predict_classes(test_image))
    Companies.append(cars[m])
    return Companies
