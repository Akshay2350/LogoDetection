from flask import Flask, json, Response, request
from image import testing

app = Flask(__name__)

@app.route('/', methods=['POST'])
def test():
    imageurl = request.json['url']
    # print(imageurl)
    prediction = testing(imageurl)
    # print(prediction)

    create_response = {
        'label':prediction
    }

    js = json.dumps(create_response)

    resp = Response(js, status=200, mimetype='application/json')
    return resp
    return "success"
